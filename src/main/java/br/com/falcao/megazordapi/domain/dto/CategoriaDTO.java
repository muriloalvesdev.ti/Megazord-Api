package br.com.falcao.megazordapi.domain.dto;

import org.springframework.util.StringUtils;

import br.com.falcao.megazordapi.domain.model.Categoria;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CategoriaDTO {

	public CategoriaDTO(Categoria categoria) {
		if (categoria != null) {
			this.codigo = categoria.getCodigo();
			this.nome = categoria.getNome();
			this.versao = categoria.getVersao();
			this.definirCampoNomeECodigo();
		}
	}

	private Long codigo;
	private String nome;
	private Integer versao;
	private String nomeECodigo;

	public Categoria toEntity() {
		Categoria categoria = new Categoria();
		categoria.setCodigo(this.getCodigo());
		categoria.setNome(this.getNome());
		categoria.setVersao(this.getVersao());
		return categoria;
	}

	private void definirCampoNomeECodigo() {
		if (!StringUtils.isEmpty(this.nome) && this.codigo != null) {
			this.nomeECodigo = new StringBuilder(String.valueOf(this.getCodigo())).append(" - ").append(this.getNome())
					.toString();
		}
	}

}
