package br.com.falcao.megazordapi.domain.filter;

import java.time.OffsetDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.util.StringUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CategoriaFilter {

	private Long codigo;
	
	private String nome;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private OffsetDateTime dataCriacao;
	
	private Boolean resumido;

	public void setNome(String nome) {
		if (!StringUtils.isEmpty(nome)) {
			this.nome = nome;
		}
	}

}
