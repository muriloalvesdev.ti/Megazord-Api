package br.com.falcao.megazordapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.falcao.megazordapi.domain.model.Categoria;
import br.com.falcao.megazordapi.repository.custom.CategoriaRepositoryCustom;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long>, CategoriaRepositoryCustom {
}
