package br.com.falcao.megazordapi.repository.custom;

import java.util.List;

import br.com.falcao.megazordapi.domain.dto.CategoriaDTO;
import br.com.falcao.megazordapi.domain.filter.CategoriaFilter;

public interface CategoriaRepositoryCustom {

	public List<CategoriaDTO> buscarCategoriasPorFiltro(CategoriaFilter categoriaFilter);

}
