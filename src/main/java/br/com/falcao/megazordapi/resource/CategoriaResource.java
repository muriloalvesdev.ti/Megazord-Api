package br.com.falcao.megazordapi.resource;

import java.util.List;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.falcao.megazordapi.domain.dto.CategoriaDTO;
import br.com.falcao.megazordapi.domain.filter.CategoriaFilter;
import br.com.falcao.megazordapi.domain.model.Categoria;
import br.com.falcao.megazordapi.service.CategoriaService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@ExposesResourceFor(Categoria.class)
@RequestMapping(path = "/categoria")
public class CategoriaResource {

	private final CategoriaService categoriaService;

	@GetMapping
	public ResponseEntity<List<CategoriaDTO>> buscarCategoriasPorFiltro(CategoriaFilter categoriaFilter) {
		return ResponseEntity.ok().body(this.categoriaService.buscarCategoriasPorFiltro(categoriaFilter));
	}

}
