package br.com.falcao.megazordapi.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.falcao.megazordapi.domain.dto.CategoriaDTO;
import br.com.falcao.megazordapi.domain.filter.CategoriaFilter;
import br.com.falcao.megazordapi.repository.CategoriaRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CategoriaService {

	private final CategoriaRepository categoriaRepository;

	public List<CategoriaDTO> buscarCategoriasPorFiltro(CategoriaFilter categoriaFilter) {
		return this.categoriaRepository.buscarCategoriasPorFiltro(categoriaFilter);
	}

}
